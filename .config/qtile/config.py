# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import psutil
import os
import subprocess
from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook, qtile
from libqtile.config import Click, Drag, Group, Key, KeyChord, Screen, Match
from libqtile.lazy import lazy

mod = "mod4"
terminal = "termite"

keys = [
        #####################
        # Window Management #
        #####################

    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down(),
        desc="Move focus down in stack pane"),
    Key([mod], "j", lazy.layout.up(),
        desc="Move focus up in stack pane"),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down(),
        desc="Move window down in current stack "),
    Key([mod, "control"], "j", lazy.layout.shuffle_up(),
        desc="Move window up in current stack "),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next(),
        desc="Switch window focus to other pane(s) of stack"),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate(),
        desc="Swap panes of split stack"),

    # Toggle between different screens
    Key([mod], "period", lazy.next_screen(), 
        desc="Next monitor"),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), 
        desc="Toggle between layouts"),

    # Increase and decrease ratio
    Key([mod], "plus", lazy.layout.grow(),
        desc="increase ratio"),
    Key([mod], "minus", lazy.layout.shrink(), desc="decrease ratio"),

    # Show all opened windows
    Key([mod], "s", lazy.spawn("rofi -theme Arc-Dark -show window -sort"),
        desc="Show all open windows"),

    ###################### 
    # Program Management #
    ######################

    # Run-Menu in Top-Bar
    Key([mod], "r", lazy.spawn("rofi -theme dmenu -show run"),
        desc="Spawn a command using rofi"),

    Key([mod], "g", lazy.spawn("/home/marcel/bin/gamechooser.sh"),
        desc="Run a game using rofi"),
    
    Key([mod], "a", lazy.spawn("/home/marcel/bin/appchooser.sh"),
        desc="Run an app using rofi"),
    
    # Opening programs simple commands
    Key([mod], "f", lazy.spawn("doublecmd"),
        desc="Launch file manager"),
    Key([mod], "m", lazy.spawn("sayonara"),
        desc="Launch music player"),
    Key([mod], "n", lazy.spawn(terminal+" -e nmtui"),
        desc="Launch networkmanager"),
    Key([mod], "t", lazy.spawn(terminal+" -e bpytop"),
        desc="Launch bpytop system monitor"),
    Key([mod], "Return", lazy.spawn(terminal), 
        desc="Launch terminal"),

    # Opening similar programs through Keychords
    ### Browser
    KeyChord([mod], "b", [
        Key([], "f", lazy.spawn("firefox")),
        Key([], "v", lazy.spawn("vimb"))
        ]),

    ### Filemanager
    KeyChord([mod], "f", [
        Key([], "m", lazy.spawn(terminal+" -e mc")),
        Key([], "d", lazy.spawn("doublecmd")),
        Key([], "p", lazy.spawn("pcmanfm"))
        ]),

    ################ 
    # Miscallenous #
    ################

    # Show Keybindings
    Key([mod, "shift"], "k", lazy.spawn(terminal+" -e /home/marcel/bin/showkeys.sh"),
        desc="show keybinds in rofi"),

    Key([mod], "w", lazy.window.kill(), 
        desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.restart(), 
        desc="Restart qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), 
        desc="Shutdown qtile"),

]

group_names = [
               ("1 SYS", {'layout': 'monadtall', 'matches':[Match(wm_class=["termite"])]}),
               ("2 FILE", {'layout': 'monadtall', 'matches':[Match(wm_class=["doublecmd", "pcmanfm"])]}),
               ("3 WORK", {'layout': 'treetab', 'matches':[Match(wm_class=["homebank", "Portfolio Performance", "/usr/bin/gnumeric", "org.remmina.Remmina"])]}),
               ("4 GAME", {'layout': 'max', 'matches':[Match(wm_class=["Minecraft* 1.18.2", "minecraft-launcher", "mono-sgen", "wesnoth"])]}),
               ("5 WEB", {'layout': 'monadtall'}),
               ("6 MEDIA", {'layout': 'ratiotile', 'matches':[Match(wm_class=["sayonara", "vlc", "TeamSpeak 3"])]}),
               ]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 3,
                "margin": 5,
                "border_focus": "74df00",
                "border_normal": "1D2330"
                }

floating_theme = {"border_width": 3,
                "border_focus": "ff5555",
                "border_normal": "1D2330"
                }

layouts = [
    layout.MonadTall(ratio=0.6, **layout_theme),
    layout.MonadWide(ratio=0.6, **layout_theme),
    layout.Max(**layout_theme),
    #layout.Stack(num_stacks=2),
    #layout.Bsp(),
    #layout.Columns(),
    layout.Matrix(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.TreeTab(font = "Ubuntu", fontsize = 12, panel_width = 170, **layout_theme),
    layout.Floating(**layout_theme),
    #layout.VerticalTile(),
    # layout.Zoomy(),
]

colors = [["#282c34", "#282c34"], # panel background: grey
          ["#434758", "#434758"], # background for current screen tab
          ["#ffffff", "#ffffff"], # font color for group names: white
          ["#ff5555", "#ff5555"], # border line color for current tab: red
          ["#8d62a9", "#8d62a9"], # border line color for other tab and odd widgets: ppurplee
          ["#668bd7", "#668bd7"]] # color for the even widgets: blue


widget_defaults = dict(
    font='Ubuntu Bold',
    fontsize=11,
    padding=2,
    background = colors[0],
)
extension_defaults = widget_defaults.copy()

screens = [
#### Middle screen        
    Screen(
            top=bar.Bar(
            [
              widget.Spacer(
                       length = 5
                       ),
              widget.Image(
                       filename = "~/.config/qtile/icons/python.png",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('rofi -theme Arc-Dark -show window'),
                                          'Button3': lambda: qtile.cmd_spawn('rofi -theme dmenu -show run')}
                       ),
              widget.Spacer(
                       length = 10
                       ),
              widget.GroupBox(
                       font = "Ubuntu Bold",
                       fontsize = 9,
                       margin_y = 3,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[2],
                       inactive = colors[2],
                       highlight_color = colors[1],
                       highlight_method = "block",
                       this_current_screen_border = colors[3],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[0],
                       other_screen_border = colors[0],
                       foreground = colors[2],
                       background = colors[0],
                       disable_drag = True
                       ),
                widget.Spacer(
                       length = 10
                       ),
                widget.Image(
                       filename = "~/.config/qtile/icons/monitor.png",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('arandr'),
                                          'Button3': lambda: qtile.cmd_spawn('arandr')}
                       ),
                widget.Spacer(
                       length = 5
                       ),
                widget.Image(
                       filename = "~/.config/qtile/icons/volume.gif",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('pavucontrol')}
                       ),
                widget.Spacer(
                       length = 5
                       ),
                widget.Image(
                       filename = "~/.config/qtile/icons/ethernet.png",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('nm-connection-editor')}
                       ),
        	widget.Spacer(),
            widget.CurrentScreen(
                    active_text = "active",
                    inactive_text = "idle",
                    inactive_color = "ff5555"
                    ),
            widget.TextBox(
                       text = '||'
                       ),
            widget.CheckUpdates(
                       update_interval = 300,
                       distro = "Arch_checkupdates", ## requires pacman-contrib
                       colour_have_updates = "ffff33",
                       colour_no_updates = "2eccfa",
                       no_update_string = "Up-to-date"
                       ),
            widget.TextBox(
                       text = '||'
                       ),
	     	widget.Memory(
		              background = colors[0],
		              foreground = "ff5555",
		              ),
                widget.TextBox(
                       text = '||'
                       ),
                widget.Clock(
                       foreground = "2eccfa",
                       background = colors[0],
                       format = "%A, %d %B [ %H:%M ]"
                       ),
                widget.TextBox(
                       text = '||'
                       ),
		widget.CurrentLayout(
                       foreground = "ff5555",
                       background = colors[0]
                       ),
		],
            20,
        ),
    ),
    Screen(
### Left screen            
        top=bar.Bar(
                [
                widget.Spacer(
                       length = 5
                       ),
                widget.GroupBox(
                       font = "Ubuntu Bold",
                       fontsize = 9,
                       margin_y = 3,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[2],
                       inactive = colors[2],
                       highlight_color = colors[1],
                       highlight_method = 'block',
                       this_current_screen_border = colors[3],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[0],
                       other_screen_border = colors[0],
                       foreground = colors[2],
                       background = colors[0],
                       disable_drag = True
                       ),
        	widget.Spacer(),
            widget.CurrentScreen(
                    active_text = "active",
                    inactive_text = "idle",
                    inactive_color = "ff5555"
                    ),
                widget.TextBox(
                       text = '||'
                       ),
		widget.CurrentLayout(
                       foreground = "ff5555",
                       background = colors[0]
                       ),
                ],
            24,
        ),
    ),
    Screen(
### Right screen            
            top=bar.Bar(
                [
                widget.Spacer(
                       length = 5
                       ),
                widget.GroupBox(
                       font = "Ubuntu Bold",
                       fontsize = 9,
                       margin_y = 3,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[2],
                       inactive = colors[2],
                       highlight_color = colors[1],
                       highlight_method = "block",
                       this_current_screen_border = colors[3],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[0],
                       other_screen_border = colors[0],
                       foreground = colors[2],
                       background = colors[0],
                       disable_drag = True
                       ),
        	widget.Spacer(),
            widget.CurrentScreen(
                    active_text = "active",
                    inactive_text = "idle",
                    inactive_color = "ff5555"
                    ),
                widget.TextBox(
                       text = '||'
                       ),
		widget.CurrentLayout(
                       foreground = "ff5555",
                       background = colors[0]
                       ),		            
                ],
            20,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(**floating_theme, float_rules=[
    *layout.Floating.default_float_rules,
    Match(wm_class='explorer.exe'), # WINE
    Match(wm_class='pavucontrol'),
    Match(wm_class='nm-connection-editor'),
    Match(wm_class='arandr')
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

